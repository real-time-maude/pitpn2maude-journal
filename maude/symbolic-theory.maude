***(
-------------
symbolic-theory.maude
-------------

This is the symbolic version of theory ./concrete-theory. However,
different from the concrete theory, two consecutive applications of tick
are not allowed (since they are already subsumed by the symbolic rule). 

Operations as mte and addFiringTimes are adjusted to consider SMT terms. 

Note: Currently the theory allows 2 consecutive applications of the rule
[applyTransition]. In order to reduce the search space, it is possible to
alternative between [tick] and [applyTransition] by using the flag (sort
TickState). 

*)

mod PETRI-NET-DYNAMICS is
   protecting STATE .

   ---------------------------------------------------
   vars T T1 T2 T3                     : Time .
   var TI                              : TimeInf .
   vars NET NET'                       : Net .
   vars M M' PRE POST INHIBIT          : Marking .
   vars PRE-M INTERMEDIATE-M POST-M    : Marking .
   vars L L'                           : Label .
   vars P P1 P2                        : Place .
   vars FT FT'                         : FiringTimes .
   var INTERVAL                        : Interval .
   vars C C'                           : BoolExpr .
   vars R1 R2                          : RExpr .
   vars R1' R2'                        : RExpr .
   var RT                              : RExpr .
   vars ST ST'                         : NState .
   var n n'                            : Nat .
   vars N1 N1'                         : IntExpr .
   var TICK                            : TickState   .
   ---------------------------------------------------

   --- Wrapper for the initial state
   eq init(NET, M, C) = { 0 , non-tick : M : zero-clock(NET) : NET , C } .

   crl [applyTransition] :
       { n ,
         TICK : --- no constrains on the flag 
         M : ((L -> R1) ; FT) : (tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET) ,
         C }
     =>
      { n ,
        non-tick : 
        POST-M :
        ((L -> 0/1) ; (updateFiringTimes(FT, PRE-M , POST-M, NET))) :
        (tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET),
        C' }
    if  PRE-M := M - PRE /\
        POST-M := (PRE-M + POST) /\
        C' := (C and 
        (PRE <= M) and (R1 in INTERVAL) and
        not inhibited(M, tr(L, PRE, POST, INHIBIT, INTERVAL)))  /\
        smtCheck(C') .

  crl [tick] :
    { n , 
      non-tick : M : FT : NET, 
      C }
     =>
     {   n + 1 ,
         tick : M : (addFiringTimes(M, FT, NET, R1)) : NET,
         C' 
    }
    if  R1 := vt(n) /\
        C' := (C and R1 >= 0/1 and mte(R1, M, FT, NET)) /\
        smtCheck(C') .

   op updateFiringTimes : FiringTimes Marking Marking Net -> FiringTimes .
   eq updateFiringTimes(empty, INTERMEDIATE-M, POST-M, NET) = empty .
   eq updateFiringTimes((L -> R1) ; FT, INTERMEDIATE-M, POST-M,
                        tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET) =
                        (L -> 
                           (((not enabled(INTERMEDIATE-M, tr(L, PRE, POST, INHIBIT, INTERVAL)))
                             and enabled(POST-M, tr(L, PRE, POST, INHIBIT, INTERVAL))) ? 
                             0/1 : --- if
                             R1)) --- else 
                             ;  updateFiringTimes(FT, INTERMEDIATE-M, POST-M, NET) .

  op mte : RExpr Marking FiringTimes Net -> BoolExpr .
  eq mte(R1 , M, (L -> T) ; FT, tr(L, PRE, POST, INHIBIT, [T1 : inf]) ; NET) =
        mte(R1, M, FT, NET) .
  eq mte(R1, M, (L -> T) ; FT, tr(L, PRE, POST, INHIBIT, [T1 : T2]) ; NET) =
         ((enabled(M, tr(L, PRE, POST, INHIBIT, [T1 : T2]))) ?
         R1 <= T2 - T : true) and mte(R1, M, FT, NET) .

  eq mte(R1, M, empty, NET) = true .


  --- strange semantics
  op addFiringTimes : Marking FiringTimes Net Time -> FiringTimes .
  eq addFiringTimes(M, (L -> T1) ; FT,
                    tr(L, PRE, POST, INHIBIT, INTERVAL) ; NET,
		    RT)
     = ( L -> (( (PRE <= M) and inhibited(M, INHIBIT)) ? T1 : T1 + RT)) ; 
       addFiringTimes(M, FT, NET, RT)
       .

  eq addFiringTimes(M, empty, NET, T) = empty .

endm
