#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import re
import csv
import math
import pandas as pd
from bs4 import BeautifulSoup
import plotly.io as pio
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

pio.kaleido.scope.mathjax = None

# timeout (5 minutes)
timeout_m = 5
TIMEOUT = timeout_m * 60 * 1000

# models to analyse
models = ["abitpro", "producer_consumer", "scheduling", "train1", "train2", "tutorial"]

# parameters
parameters = ["p0", "p1", "p2"]

# maude solvers
solvers = ["base", "yices2", "z3", "cvc4"]

# colors and markers
colors = ["#3366CC", "#109618", "#DC3912", "#FF9900"]
markers = ["circle-open", "star-open", "square-open", "star-diamond-open"]

# maude theories
theories = [
    "search-sym",
    "search-sym2",
    "search-folding",
    "folding",
]

safety_theory = "AG-synthesis"

# csv with results
csv_filename_format = "results_{place}.csv"
safety_csv_filename = "results_safety.csv"

# folder with tools results
log_folder_format = "logs-{place}"

# folder with the models
models_folder = "models"

# filenames
model_file = "{model}.xml"
romeo_file = "{model}.cts.{place}.res"
maude_file = "{model}.{theory}.maude_{solver}.{place}.res"
safety_romeo_file = "{model}.1safe.cts.res"
safety_maude_file = "{model}.AG-synthesis.maude_{solver}.res"

# templates
maude_template = "{theory}_{solver}_maude(ms)"
maude_fieldnames = [
    maude_template.format(theory=t, solver=s) for t in theories for s in solvers
]
tools_fieldnames = ["romeo(ms)"] + maude_fieldnames

safety_tools_fieldnames = ["romeo(ms)"] + [
    maude_template.format(theory=safety_theory, solver=s) for s in solvers
]


# # Generate CSV file with data
# 

# In[2]:


def format_unit(value, unit):
    """Convert value into milliseconds"""
    if unit == "ms":
        return float(value)
    elif unit == "second" or unit == "seconds" or unit == "s":
        return float(value) * 1000
    else:
        raise Exception(f"Unit {unit} is not supported")


def is_parameter(value):
    return value != "inf" and re.match(r"^-?\d+(?:\.\d+)?$", value) is None


def get_parameters(transitions):
    t_intervals = []
    for t in transitions:
        lb = t["eft_param"] if t.has_attr("eft_param") else t["eft"]
        ub = t["lft_param"] if t.has_attr("lft_param") else t["lft"]
        t_intervals.append((lb, ub))

    return list(filter(is_parameter, set(sum(t_intervals, ()))))


def generate_csv(place):
    regex_timeout = re.compile(r"timeout")
    regex_romeo = re.compile(r"real\s*(\d+m\d+\.\d+s)")
    regex_maude = re.compile(r"rewrites: (\d+) in (\d+)(\w+) cpu \((\d+)(\w+) real\)")

    # log files
    log_folder = log_folder_format.format(place=place)
    csv_filename = csv_filename_format.format(place=place)

    with open(csv_filename, "w") as csv_file:
        fieldnames = [
            "model",
            "parameters",
            "places",
            "transitions",
            "arcs",
            "place_reached",
        ]
        fieldnames += tools_fieldnames

        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for model in models:
            # search model information
            nb_parameters = ""
            nb_places = ""
            nb_transitions = ""
            nb_arcs = ""
            model_filename = model_file.format(model=model)
            places = []
            with open(os.path.join(models_folder, model_filename), "r") as m_file:
                soup = BeautifulSoup(m_file, "xml")
                places = [p["label"] for p in soup.find_all("place")]
                nb_places = len(places)
                nb_arcs = len(soup.find_all("arc"))
                transitions = soup.find_all("transition")
                nb_transitions = len(transitions)
                nb_parameters = len(get_parameters(transitions))

            for place in places:
                # search in romeo file
                romeo_time = ""
                romeo_filename = romeo_file.format(model=model, place=place)
                romeo_path = os.path.join(log_folder, model, romeo_filename)
                if os.path.exists(romeo_path):
                    with open(romeo_path, "r") as r_file:
                        romeo_content = r_file.read()
                        if (
                            len(romeo_content)
                            and regex_timeout.search(romeo_content) is None
                        ):
                            _romeo = regex_romeo.search(romeo_content).groups()
                            romeo_time = str(
                                pd.to_timedelta(_romeo)
                                .total_seconds()[0]
                                .astype("float")
                            )
                            romeo_time = f'{format_unit(romeo_time, "seconds")}'

                # search in maude files
                maude_times = []
                for theory in theories:
                    for solver in solvers:
                        maude_time = ""
                        maude_filename = maude_file.format(
                            model=model, place=place, theory=theory, solver=solver
                        )
                        maude_path = os.path.join(log_folder, model, maude_filename)
                        if os.path.exists(maude_path):
                            with open(maude_path, "r") as rl_file:
                                maude_content = rl_file.read()
                                if (
                                    len(maude_content)
                                    and regex_timeout.search(maude_content) is None
                                ):
                                    _, _, _, _maude, _maude_unit = regex_maude.search(
                                        maude_content
                                    ).groups()
                                    maude_time = f"{format_unit(_maude, _maude_unit)}"
                                    maude_times.append(
                                        (
                                            maude_template.format(
                                                theory=theory, solver=solver
                                            ),
                                            maude_time,
                                        )
                                    )
                        else:
                            print(f"file {maude_path} does not exist")

                maude_times = dict(maude_times)

                # save info
                writer.writerow(
                    {
                        "model": model,
                        "place_reached": place,
                        "parameters": nb_parameters,
                        "places": nb_places,
                        "transitions": nb_transitions,
                        "arcs": nb_arcs,
                        "romeo(ms)": romeo_time,
                        **maude_times,
                    }
                )


# In[3]:


def generate_safety_csv(log_folder):
    regex_timeout = re.compile(r"timeout")
    regex_romeo = re.compile(r"real\s*(\d+m\d+\.\d+s)")
    regex_maude = re.compile(r"rewrites: (\d+) in (\d+)(\w+) cpu \((\d+)(\w+) real\)")

    with open(safety_csv_filename, "w") as csv_file:
        fieldnames = ["model"] + safety_tools_fieldnames

        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for model in models:
            # search in romeo file
            romeo_time = ""
            romeo_filename = safety_romeo_file.format(model=model)
            romeo_path = os.path.join(log_folder, model, romeo_filename)
            if os.path.exists(romeo_path):
                with open(romeo_path, "r") as r_file:
                    romeo_content = r_file.read()
                    if (
                        len(romeo_content)
                        and regex_timeout.search(romeo_content) is None
                    ):
                        _romeo = regex_romeo.search(romeo_content).groups()
                        romeo_time = str(
                            pd.to_timedelta(_romeo).total_seconds()[0].astype("float")
                        )
                        romeo_time = f'{format_unit(romeo_time, "seconds")}'

            # search in maude files
            maude_times = []
            for solver in solvers:
                maude_time = ""
                maude_filename = safety_maude_file.format(model=model, solver=solver)
                maude_path = os.path.join(log_folder, model, maude_filename)
                if os.path.exists(maude_path):
                    with open(maude_path, "r") as rl_file:
                        maude_content = rl_file.read()
                        if (
                            len(maude_content)
                            and regex_timeout.search(maude_content) is None
                        ):
                            _, _, _, _maude, _maude_unit = regex_maude.search(
                                maude_content
                            ).groups()
                            maude_time = f"{format_unit(_maude, _maude_unit)}"
                            maude_times.append(
                                (
                                    maude_template.format(
                                        theory=safety_theory, solver=solver
                                    ),
                                    maude_time,
                                )
                            )
                else:
                    print(f"file {maude_path} does not exist")

            maude_times = dict(maude_times)

            # save info
            writer.writerow({"model": model, "romeo(ms)": romeo_time, **maude_times})


# In[4]:


[generate_csv(p) for p in parameters]


# In[5]:


generate_safety_csv(log_folder_format.format(place="p2"))


# # Analyse Data
# 

# In[6]:


def highlight_cell(
    df, latex=False, subset=tools_fieldnames, null_color="red", min_color="green"
):
    color_fmt = "color: {{{color}}}; bfseries: ;" if latex else "color: {color};"
    return (
        df.style.highlight_min(
            axis=1, props=color_fmt.format(color=min_color), subset=subset
        )
        .highlight_null(props=color_fmt.format(color=null_color))
        .format(na_rep="TO", precision=1)
    )


# In[7]:


dfs = {
    p: pd.read_csv(
        csv_filename_format.format(place=p), float_precision="round_trip"
    ).set_index("model")
    for p in parameters
}
df = dfs[list(dfs)[0]]
df


# In[8]:


dfs_times = {
    k: df.reset_index().drop(
        labels=["parameters", "places", "transitions", "arcs"], axis=1
    )
    for (k, df) in dfs.items()
}
highlight_cell(dfs_times["p1"])


# In[9]:


df_safety = pd.read_csv(safety_csv_filename, float_precision="round_trip")
df_safety = df_safety.set_index("model")
highlight_cell(df_safety, False, safety_tools_fieldnames)


# In[10]:


df_model_info = df.drop_duplicates(
    subset=["parameters", "places", "transitions", "arcs"], keep="last"
).reset_index()
df_model_info = df_model_info.drop(labels=["place_reached"] + tools_fieldnames, axis=1)
df_model_info = df_model_info.sort_values(
    by="model", key=lambda col: col.str.lower(), ignore_index=True
)
df_model_info


# # Plot
# 

# In[11]:


def plot(dfs, model_name):
    subplot_titles = [f"{t}" for t in theories]
    fig = make_subplots(
        rows=1,
        cols=len(theories),
        subplot_titles=subplot_titles,
        x_title="Maude (ms)",
        y_title="Romeo (ms)",
    )

    # computes axis bound
    axis_bound = math.ceil(math.log10(TIMEOUT))

    # results
    for t_i, t in enumerate(theories):
        for s_i, s in enumerate(solvers):
            maude_t = maude_template.format(theory=t, solver=s)
            for p_i, (k, df) in enumerate(dfs.items()):
                model = df.fillna(TIMEOUT).loc[[model_name]]
                if maude_t in model:
                    fig.add_trace(
                        go.Scatter(
                            x=model[maude_t],
                            y=model["romeo(ms)"],
                            text=model["place_reached"],
                            mode="markers",
                            marker=dict(
                                symbol=markers[s_i],  # compare solvers
                                color=colors[s_i],
                                size=20,
                                line=dict(width=2),
                            ),
                            name=s,
                            showlegend=t_i == 0 and p_i == 0,
                            hovertemplate=f"<b>EF (%{{text}} > {p_i})</b><br><br><br>Maude: %{{x}} ms <br>Romeo: %{{y}} ms<extra></extra>",
                        ),
                        row=1,
                        col=t_i + 1,
                    )

        # timeout lines
        fig.add_trace(
            go.Scatter(
                x=[0, TIMEOUT],
                y=[TIMEOUT, TIMEOUT],
                mode="lines",
                line=dict(color="grey", width=1, dash="dash"),
                showlegend=False,
            ),
            row=1,
            col=t_i + 1,
        )
        fig.add_trace(
            go.Scatter(
                x=[TIMEOUT, TIMEOUT],
                y=[0, TIMEOUT],
                mode="lines",
                line=dict(color="grey", width=1, dash="dash"),
                showlegend=False,
            ),
            row=1,
            col=t_i + 1,
        )

        fig.add_annotation(
            x=0.55,
            y=1.01,
            xref="x domain",
            yref="y domain",
            text=f"timeout ({timeout_m} min.)",
            font=dict(size=14, color="grey"),
            showarrow=False,
            opacity=0.5,
            row=1,
            col=t_i + 1,
        )
        fig.add_annotation(
            x=1.01,
            y=0.5,
            xref="x domain",
            yref="y domain",
            text=f"timeout ({timeout_m} min.)",
            font=dict(size=14, color="grey"),
            showarrow=False,
            opacity=0.5,
            textangle=90,
            row=1,
            col=t_i + 1,
        )

        # identity line
        fig.add_trace(
            go.Scatter(
                x=[0, 10 ** (axis_bound + 0.2)],
                y=[0, 10 ** (axis_bound + 0.2)],
                mode="lines",
                line=dict(color="black", width=1),
                showlegend=False,
            ),
            row=1,
            col=t_i + 1,
        )

        # update axis
        fig.update_xaxes(
            type="log",
            showgrid=True,
            mirror=True,
            linewidth=1,
            linecolor="black",
            constrain="domain",
            range=[-1, axis_bound + 0.2],
            dtick=2,
            tick0=0,
            row=1,
            col=t_i + 1,
        )
        fig.update_yaxes(
            type="log",
            showgrid=True,
            mirror=True,
            linewidth=1,
            linecolor="black",
            scaleanchor="x",
            scaleratio=1,
            range=[-1, axis_bound + 0.2],
            dtick=2,
            tick0=0,
            row=1,
            col=t_i + 1,
        )

    # update legends
    legend_options = dict(
        # yanchor="top",
        # y=0.99,
        # xanchor="left",
        # x=0.1,
        bordercolor="Black",
        borderwidth=1,
    )
    margin = dict(l=60, r=0, t=20, b=60)
    fig.update_layout(
        width=1200,
        height=320,
        paper_bgcolor="white",
        plot_bgcolor="white",
        legend_title_text="Solver",
        legend=legend_options,
        autosize=False,
        margin=margin,
        font=dict(size=18),
        showlegend=False,
    )

    return fig


# show a figure
plot(dfs, models[0])


# In[12]:


def plot_safety(df_safety, model_names):
    # computes axis bound
    axis_bound = math.ceil(math.log10(TIMEOUT))
    nb_cols = int(len(model_names) / 2)
    fig = make_subplots(
        rows=2,
        cols=nb_cols,
        subplot_titles=model_names,
        vertical_spacing=0.15,
        x_title="Maude (ms)",
        y_title="Romeo (ms)",
    )

    # results
    for col, model_name in enumerate(model_names):
        row = int(col / nb_cols) + 1
        col = (col % nb_cols) + 1
        # print(row, col, model_name)
        for s_i, s in enumerate(solvers):
            maude_t = maude_template.format(theory=safety_theory, solver=s)
            safety_model = df_safety.fillna(TIMEOUT).loc[[model_name]]
            if maude_t in safety_model:
                fig.add_trace(
                    go.Scatter(
                        x=safety_model[maude_t],
                        y=safety_model["romeo(ms)"],
                        text="1-safe",
                        mode="markers",
                        marker=dict(
                            symbol=markers[s_i],  # compare solvers
                            color=colors[s_i],
                            size=20,
                            line=dict(width=2),
                        ),
                        name=s,
                        showlegend=col == 0,
                        hovertemplate="<b>1-safe</b><br><br>Maude: %{x} ms <br>Romeo: %{y} ms<extra></extra>",
                    ),
                    row=row,
                    col=col,
                )

        # timeout lines
        fig.add_trace(
            go.Scatter(
                x=[0, TIMEOUT],
                y=[TIMEOUT, TIMEOUT],
                mode="lines",
                line=dict(color="grey", width=1, dash="dash"),
                showlegend=False,
            ),
            row=row,
            col=col,
        )
        fig.add_trace(
            go.Scatter(
                x=[TIMEOUT, TIMEOUT],
                y=[0, TIMEOUT],
                mode="lines",
                line=dict(color="grey", width=1, dash="dash"),
                showlegend=False,
            ),
            row=row,
            col=col,
        )

        fig.add_annotation(
            x=0.55,
            y=1.01,
            xref="x domain",
            yref="y domain",
            text=f"timeout ({timeout_m} min.)",
            font=dict(size=14, color="grey"),
            showarrow=False,
            opacity=0.5,
            row=row,
            col=col,
        )
        fig.add_annotation(
            x=1.01,
            y=0.5,
            xref="x domain",
            yref="y domain",
            text=f"timeout ({timeout_m} min.)",
            font=dict(size=14, color="grey"),
            showarrow=False,
            opacity=0.5,
            textangle=90,
            row=row,
            col=col,
        )

        # identity line
        fig.add_trace(
            go.Scatter(
                x=[0, 10 ** (axis_bound + 0.2)],
                y=[0, 10 ** (axis_bound + 0.2)],
                mode="lines",
                line=dict(color="black", width=1),
                showlegend=False,
            ),
            row=row,
            col=col,
        )

        # update axis
        fig.update_xaxes(
            type="log",
            showgrid=True,
            mirror=True,
            linewidth=1,
            linecolor="black",
            constrain="domain",
            range=[-1, axis_bound + 0.2],
            dtick=2,
            tick0=0,
            row=row,
            col=col,
        )
        fig.update_yaxes(
            type="log",
            showgrid=True,
            mirror=True,
            linewidth=1,
            linecolor="black",
            scaleanchor="x",
            scaleratio=1,
            range=[-1, axis_bound + 0.2],
            dtick=2,
            tick0=0,
            row=row,
            col=col,
        )

    # update legends
    legend_options = dict(
        # yanchor="top",
        # y=0.99,
        # xanchor="left",
        # x=0.1,
        bordercolor="Black",
        borderwidth=1,
    )
    margin = dict(l=60, r=0, t=20, b=60)
    fig.update_layout(
        width=960,
        height=660,
        paper_bgcolor="white",
        plot_bgcolor="white",
        legend_title_text="Solver",
        legend=legend_options,
        autosize=False,
        margin=margin,
        font=dict(size=18),
        showlegend=False,
    )

    return fig


# show a figure
plot_safety(df_safety, models)


# In[13]:


fig = plot_safety(df_safety, models)
fig.write_html("images/safety.html")
fig.write_image("images/safety.pdf", format="pdf")


# In[14]:


def export_to_latex(
    df,
    filename,
    highlight=True,
    subset=tools_fieldnames,
    null_color="BrickRed",
    min_color="OliveGreen",
):
    base_style = (
        highlight_cell(df, True, subset, null_color, min_color)
        if highlight
        else df.style
    )
    s = base_style.format_index("\\textbf{{{}}}", escape="latex", axis=1).hide(
        axis="index"
    )

    return s.to_latex(os.path.join("images", f"{filename}.tex"), hrules=True)


# In[15]:


for k, df in dfs_times.items():
    export_to_latex(df, f"table-times-{k}")


# In[16]:


export_to_latex(
    df_safety.reset_index(), "table-times-safety", True, safety_tools_fieldnames
)
export_to_latex(df_model_info, "table-params", False)


# In[17]:


for m in models:
    fig = plot(dfs, m)
    filename = m.replace("_", "-")
    fig.write_html(f"images/{filename}.html")
    fig.write_image(f"images/{filename}.pdf", format="pdf")


# # Compare Theories
# 

# In[18]:


def plot_theories(dfs, model_name):
    theories_vs = [("search-sym", "search-sym2"), ("search-folding", "folding")]
    # subplot_titles = [f"{t[0]} vs {t[1]}" for t in theories_vs]
    fig = make_subplots(
        rows=1,
        cols=len(theories_vs),
        horizontal_spacing=0.2,
        column_widths=[0.5, 0.4],
        # subplot_titles=subplot_titles,
        # x_title="Maude (ms)",
        # y_title="Maude (ms)",
    )

    # computes axis bound
    axis_bound = math.ceil(math.log10(TIMEOUT))

    # results
    for t_i, (t1, t2) in enumerate(theories_vs):
        for s_i, s in enumerate(solvers):
            maude_t1 = maude_template.format(theory=t1, solver=s)
            maude_t2 = maude_template.format(theory=t2, solver=s)
            for p_i, (k, df) in enumerate(dfs.items()):
                model = df.fillna(TIMEOUT).loc[[model_name]]
                if maude_t1 in model and maude_t2 in model:
                    fig.add_trace(
                        go.Scatter(
                            x=model[maude_t1],
                            y=model[maude_t2],
                            text=model["place_reached"],
                            mode="markers",
                            marker=dict(
                                symbol=markers[s_i],  # compare solvers
                                color=colors[s_i],
                                size=20,
                                line=dict(width=2),
                            ),
                            name=s,
                            showlegend=t_i == 0 and p_i == 0,
                            hovertemplate=f"<b>EF (%{{text}} > {p_i})</b><br><br><br>{t1}: %{{x}} ms <br>{t2}: %{{y}} ms<extra></extra>",
                        ),
                        row=1,
                        col=t_i + 1,
                    )

        # timeout lines
        fig.add_trace(
            go.Scatter(
                x=[0, TIMEOUT],
                y=[TIMEOUT, TIMEOUT],
                mode="lines",
                line=dict(color="grey", width=1, dash="dash"),
                showlegend=False,
            ),
            row=1,
            col=t_i + 1,
        )
        fig.add_trace(
            go.Scatter(
                x=[TIMEOUT, TIMEOUT],
                y=[0, TIMEOUT],
                mode="lines",
                line=dict(color="grey", width=1, dash="dash"),
                showlegend=False,
            ),
            row=1,
            col=t_i + 1,
        )

        fig.add_annotation(
            x=0.55,
            y=1.01,
            xref="x domain",
            yref="y domain",
            text=f"timeout ({timeout_m} min.)",
            font=dict(size=14, color="grey"),
            showarrow=False,
            opacity=0.5,
            row=1,
            col=t_i + 1,
        )
        fig.add_annotation(
            x=1.01,
            y=0.5,
            xref="x domain",
            yref="y domain",
            text=f"timeout ({timeout_m} min.)",
            font=dict(size=14, color="grey"),
            showarrow=False,
            opacity=0.5,
            textangle=90,
            row=1,
            col=t_i + 1,
        )

        # identity line
        fig.add_trace(
            go.Scatter(
                x=[0, 10 ** (axis_bound + 0.2)],
                y=[0, 10 ** (axis_bound + 0.2)],
                mode="lines",
                line=dict(color="black", width=1),
                showlegend=False,
            ),
            row=1,
            col=t_i + 1,
        )

        # update axis
        fig.update_xaxes(
            type="log",
            showgrid=True,
            mirror=True,
            linewidth=1,
            linecolor="black",
            constrain="domain",
            range=[-1, axis_bound + 0.2],
            dtick=2,
            tick0=0,
            row=1,
            col=t_i + 1,
        )
        fig.update_yaxes(
            type="log",
            showgrid=True,
            mirror=True,
            linewidth=1,
            linecolor="black",
            scaleanchor="x",
            scaleratio=1,
            range=[-1, axis_bound + 0.2],
            dtick=2,
            tick0=0,
            row=1,
            col=t_i + 1,
        )

        fig["layout"][f"xaxis{t_i+1}"]["title"] = t1
        fig["layout"][f"yaxis{t_i+1}"]["title"] = t2

    # update legends
    legend_options = dict(
        # yanchor="top",
        # y=0.99,
        # xanchor="left",
        # x=0.1,
        bordercolor="Black",
        borderwidth=1,
    )
    margin = dict(l=60, r=0, t=10, b=20)
    fig.update_layout(
        width=800,
        height=320,
        paper_bgcolor="white",
        plot_bgcolor="white",
        legend_title_text="Solver",
        legend=legend_options,
        autosize=False,
        margin=margin,
        font=dict(size=18),
        showlegend=False,
    )

    return fig


plot_theories(dfs, models[0])


# In[19]:


for m in models:
    fig = plot_theories(dfs, m)
    filename = m.replace("_", "-")
    fig.write_html(f"images/comparison-theories-{filename}.html")
    fig.write_image(f"images/comparison-theories-{filename}.pdf", format="pdf")


# # Old Version VS New Version
# 

# In[20]:


# csv with results
old_csv_filename_format = "old_results/conference_version/results_{place}.csv"
old_safety_csv_filename = "old_results/conference_version/results_safety.csv"

old_dfs = {
    p: pd.read_csv(
        old_csv_filename_format.format(place=p), float_precision="round_trip"
    ).set_index("model")
    for p in parameters
}
old_df = old_dfs[list(old_dfs)[0]]
old_df


# In[21]:


old_df_safety = pd.read_csv(old_safety_csv_filename, float_precision="round_trip")
old_df_safety = old_df_safety.set_index("model")
old_df_safety


# In[22]:


old_models = ["producer_consumer", "scheduling", "tutorial"]


# In[23]:


def plot_comparison(dfs, old_dfs, model_names):
    # computes axis bound
    axis_bound = math.ceil(math.log10(TIMEOUT))

    # maude solvers
    plot_solver = "z3"

    # maude theories
    t_new, t_old = ("search-folding", "symbolic-folding-tree")

    fig = make_subplots(
        rows=1,
        cols=len(model_names),
        subplot_titles=model_names,
        x_title="Current Implementation (ms)",
        y_title="Previous Implementation (ms)",
    )

    key_plots = dfs.keys()
    for col, model_name in enumerate(model_names):
        new_maude_t = maude_template.format(theory=t_new, solver=plot_solver)
        old_maude_t = maude_template.format(theory=t_old, solver=plot_solver)
        for key in key_plots:  # reached place
            old_model = old_dfs[key].fillna(TIMEOUT).loc[[model_name]]
            new_model = dfs[key].fillna(TIMEOUT).loc[[model_name]]
            if new_maude_t in new_model and old_maude_t in old_model:
                fig.add_trace(
                    go.Scatter(
                        x=new_model[new_maude_t],
                        y=old_model[old_maude_t],
                        text=new_model["place_reached"],
                        mode="markers",
                        marker=dict(
                            symbol=markers[2],  # compare solvers
                            color=colors[2],
                            size=20,
                            line=dict(width=2),
                        ),
                        name=plot_solver,
                        # showlegend=t_i == 0 and key == "p0",
                        hovertemplate=f"<b>EF (%{{text}} > {key[-1]})</b><br><br><br>New: %{{x}} ms <br>Old: %{{y}} ms<extra></extra>",
                    ),
                    row=1,
                    col=col + 1,
                )

        # timeout lines
        fig.add_trace(
            go.Scatter(
                x=[0, TIMEOUT],
                y=[TIMEOUT, TIMEOUT],
                mode="lines",
                line=dict(color="grey", width=1, dash="dash"),
                showlegend=False,
            ),
            row=1,
            col=col + 1,
        )
        fig.add_trace(
            go.Scatter(
                x=[TIMEOUT, TIMEOUT],
                y=[0, TIMEOUT],
                mode="lines",
                line=dict(color="grey", width=1, dash="dash"),
                showlegend=False,
            ),
            row=1,
            col=col + 1,
        )

        fig.add_annotation(
            x=0.55,
            y=1.01,
            xref="x domain",
            yref="y domain",
            text=f"timeout ({timeout_m} min.)",
            font=dict(size=14, color="grey"),
            showarrow=False,
            opacity=0.5,
            row=1,
            col=col + 1,
        )
        fig.add_annotation(
            x=1.01,
            y=0.5,
            xref="x domain",
            yref="y domain",
            text=f"timeout ({timeout_m} min.)",
            font=dict(size=14, color="grey"),
            showarrow=False,
            opacity=0.5,
            textangle=90,
            row=1,
            col=col + 1,
        )

        # identity line
        fig.add_trace(
            go.Scatter(
                x=[0, 10 ** (axis_bound + 0.2)],
                y=[0, 10 ** (axis_bound + 0.2)],
                mode="lines",
                line=dict(color="black", width=1),
                showlegend=False,
            ),
            row=1,
            col=col + 1,
        )

        # update axis
        fig.update_xaxes(
            type="log",
            showgrid=True,
            mirror=True,
            linewidth=1,
            linecolor="black",
            constrain="domain",
            range=[-1, axis_bound + 0.2],
            dtick=2,
            tick0=0,
            row=1,
            col=col + 1,
        )
        fig.update_yaxes(
            type="log",
            showgrid=True,
            mirror=True,
            linewidth=1,
            linecolor="black",
            scaleanchor="x",
            scaleratio=1,
            range=[-1, axis_bound + 0.2],
            dtick=2,
            tick0=0,
            row=1,
            col=col + 1,
        )

        # update legends
        legend_options = dict(
            bordercolor="Black",
            borderwidth=1,
        )

        margin = dict(l=60, r=0, t=20, b=60)
        fig.update_layout(
            width=900,
            height=320,
            paper_bgcolor="white",
            plot_bgcolor="white",
            legend_title_text="Solver",
            legend=legend_options,
            showlegend=False,
            autosize=False,
            margin=margin,
            font=dict(size=18),
        )
    return fig


# show a figure
plot_comparison(dfs, old_dfs, old_models)


# In[24]:


fig = plot_comparison(dfs, old_dfs, old_models)
fig.write_html("images/maude-comparison.html")
fig.write_image("images/maude-comparison.pdf", format="pdf")

