// TPN name=/Users/himito/Work/code/esprits/pitpn2maude-journal/benchmarks/models/abitpro.cts
// insert here your type definitions using C-like syntax


// insert here your function definitions
// using C-like syntax






initially {
 // insert here the state variables declarations
// and possibly some code to initialize them
// using C-like syntax


int  P1=1, P2=0, P3=0, P4=0, P5=1, P6=0, P7=0, P8=0, P9=0, P10=0, P11=0, P12=0; }

 transition [ intermediate {   P1 =  P1  - 1; }]  T1 [0,inf]
      when (P1 >= 1)
      {   P1 = P1  - 1 , P2 = P2 + 1 , P9 = P9 + 1; }
 transition [ intermediate {   P2 =  P2  - 1; }]  T2 [5,6]
      when (P2 >= 1)
      {   P2 =  P2  - 1 +  1 , P9 = P9 + 1; }
 transition [ intermediate {   P2 =  P2  - 1 , P10 =  P10  - 1; }]  T3 [0,1]
      when (P10 >= 1 and P2 >= 1)
      {   P2 = P2  - 1 , P10 = P10  - 1 , P3 = P3 + 1; }
 transition [ intermediate {   P3 =  P3  - 1; }]  T4 [0,inf]
      when (P3 >= 1)
      {   P3 = P3  - 1 , P4 = P4 + 1 , P11 = P11 + 1; }
 transition [ intermediate {   P4 =  P4  - 1; }]  T5 [5,6]
      when (P4 >= 1)
      {   P4 =  P4  - 1 +  1 , P11 = P11 + 1; }
 transition [ intermediate {   P4 =  P4  - 1 , P12 =  P12  - 1; }]  T6 [0,1]
      when (P12 >= 1 and P4 >= 1)
      {   P4 = P4  - 1 , P12 = P12  - 1 , P1 = P1 + 1; }
 transition [ intermediate {   P5 =  P5  - 1 , P9 =  P9  - 1; }]  T7 [0,1]
      when (P9 >= 1 and P5 >= 1)
      {   P5 = P5  - 1 , P9 = P9  - 1 , P6 = P6 + 1; }
 transition [ intermediate {   P6 =  P6  - 1; }]  T8 [0,2]
      when (P6 >= 1)
      {   P6 = P6  - 1 , P7 = P7 + 1 , P10 = P10 + 1; }
 transition [ intermediate {   P9 =  P9  - 1 , P7 =  P7  - 1; }]  T9 [0,1]
      when (P7 >= 1 and P9 >= 1)
      {   P9 = P9  - 1 , P7 = P7  - 1 , P6 = P6 + 1; }
 transition [ intermediate {   P7 =  P7  - 1 , P11 =  P11  - 1; }]  T10 [0,1]
      when (P11 >= 1 and P7 >= 1)
      {   P7 = P7  - 1 , P11 = P11  - 1 , P8 = P8 + 1; }
 transition [ intermediate {   P8 =  P8  - 1; }]  T11 [0,2]
      when (P8 >= 1)
      {   P8 = P8  - 1 , P5 = P5 + 1 , P12 = P12 + 1; }
 transition [ intermediate {   P11 =  P11  - 1 , P5 =  P5  - 1; }]  T12 [0,1]
      when (P5 >= 1 and P11 >= 1)
      {   P11 = P11  - 1 , P5 = P5  - 1 , P8 = P8 + 1; }
 transition [ intermediate {   P9 =  P9  - 1; }]  T13 [0,1]
      when (P9 >= 1)
      {   P9 = P9  - 1; }
 transition [ intermediate {   P10 =  P10  - 1; }]  T14 [0,1]
      when (P10 >= 1)
      {   P10 = P10  - 1; }
 transition [ intermediate {   P11 =  P11  - 1; }]  T15 [0,1]
      when (P11 >= 1)
      {   P11 = P11  - 1; }
 transition [ intermediate {   P12 =  P12  - 1; }]  T16 [0,1]
      when (P12 >= 1)
      {   P12 = P12  - 1; }


  // insert TCTL formula here : check formula
  check [ timed_trace, restrict] EF ( <replace> )