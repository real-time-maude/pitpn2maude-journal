// TPN name=/Users/himito/Work/code/esprits/pitpn2maude-journal/benchmarks/models/train1.cts
// insert here your type definitions using C-like syntax


// insert here your function definitions
// using C-like syntax






initially {
 // insert here the state variables declarations
// and possibly some code to initialize them
// using C-like syntax


int  in=0, far=1, coming=0, leaving=0, open=1, lowering=0, raising=0, closed=0, close1=0, on1=0, left1=0, far1=1; }

 transition [ intermediate {   far =  far  - 1 , far1 =  far1  - 1; }]  App [0,inf]
      when (far1 >= 1 and far >= 1)
      {   far = far  - 1 , far1 = far1  - 1 , coming = coming + 1 , in = in + 1 , close1 = close1 + 1; }
 transition [ intermediate {   in =  in  - 1 , far =  far  - 1; }]  App2 [0,inf]
      when (far >= 1 and in >= 1)
      {   in =  in  - 1 +  2 , far = far  - 1; }
 transition [ intermediate {   in =  in  - 2; }]  Exit1 [0,0]
      when (in >= 2)
      {   in =  in  - 2 +  1 , far = far + 1; }
 transition [ intermediate {   in =  in  - 1 , left1 =  left1  - 1; }]  Exit [0,0]
      when (left1 >= 1 and in >= 1)
      {   in = in  - 1 , left1 = left1  - 1 , far = far + 1 , leaving = leaving + 1 , far1 = far1 + 1; }
 transition [ intermediate {   open =  open  - 1 , coming =  coming  - 1; }]  Down1 [0,0]
      when (coming >= 1 and open >= 1)
      {   open = open  - 1 , coming = coming  - 1 , lowering = lowering + 1; }
 transition [ intermediate {   raising =  raising  - 1; }]  R [1,2]
      when (raising >= 1)
      {   raising = raising  - 1 , open = open + 1; }
 transition [ intermediate {   raising =  raising  - 1 , coming =  coming  - 1; }]  Down [0,0]
      when (coming >= 1 and raising >= 1)
      {   raising = raising  - 1 , coming = coming  - 1 , lowering = lowering + 1; }
 transition [ intermediate {   lowering =  lowering  - 1; }]  L [1,2]
      when (lowering >= 1)
      {   lowering = lowering  - 1 , closed = closed + 1; }
 transition [ intermediate {   closed =  closed  - 1 , leaving =  leaving  - 1; }]  up [0,0]
      when (leaving >= 1 and closed >= 1)
      {   closed = closed  - 1 , leaving = leaving  - 1 , raising = raising + 1; }
 transition [ intermediate {   close1 =  close1  - 1; }]  In1 [3,5]
      when (close1 >= 1)
      {   close1 = close1  - 1 , on1 = on1 + 1; }
 transition [ intermediate {   on1 =  on1  - 1; }]  Ex1 [2,4]
      when (on1 >= 1)
      {   on1 = on1  - 1 , left1 = left1 + 1; }


  // insert TCTL formula here : check formula
  check [ timed_trace, restrict] EF ( <replace> )