# Benchmarks

This folder contains the scripts to generate the benchmarks of the paper.

## Structure of the folder

```
.
├── analysis.ipynb    # Python notebook to generate the plots (analysis.py)
├── benchmarks.sh     # Bash script to run the benchmarks
├── images            # Folder containing the plots of the benchmark results
├── models            # Folder containing the models of the benchmark
├── requirements.txt  # File containing the python requirements
└── results_p{n}.csv   # CSV file with the benchmarks results where EF(p > n)
```

## Run benchmarks

The script `benchmarks.sh` allows to run the benchmarks for a specific model.
Before running it, please set the variables `romeo` and `maude_solvers` with the
absolute path of Romeo and Maude binaries, respectively.

```
λ> ./benchmarks.sh
./benchmarks.sh -m MODEL -t TIMEOUT [-n N_TOKENS] [-s]
```

Use the flag `-s` to run the benchmarks related to the 1-safe property. The
argument `n` corresponds to the benchmarks of the property `EF(p > n)`.
